<?php

namespace Drupal\aws_sqs_api\Queue;

use Aws\Sqs\Exception\SqsException;
use Drupal\aws_sqs_api\AwsSqsQueueItem;
use Drupal\Core\Utility\Error;

/**
 * Implementation of the methods required by the QueueInterface.
 *
 * @see \Drupal\Core\Queue\QueueInterface
 */
trait DrupalQueueInterfaceImplementationTrait {

  /**
   * {@inheritdoc}
   */
  public function createItem($data) {
    $sqsPayload = [
      'QueueUrl' => $this->getQueueUrl(),
      'MessageBody' => ($this->serializationFormat) ? $this->serialize($data) : $data,
    ];

    // SQS requires additional properties for reliable (FIFO) queues.
    if ($this->isReliable()) {
      // Messages with the same MessageGroupId are returned in sequence.
      // Messages with different MessageGroupId values can be fetched in
      // parallel.
      $sqsPayload['MessageGroupId'] = self::reliableQueueMessageGroupId($data);
      $sqsPayload['MessageDeduplicationId'] = self::reliableQueueMessageDeduplicationId($data);
    }
    $result = $this->getClient()->sendMessage($sqsPayload);

    return $result->get('MessageId') ?? FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function numberOfItems() {
    try {
      return $this->attr('ApproximateNumberOfMessages');
    }
    catch (CredentialsException $e) {
      // If the credentials are not configured, treat the queue as empty.
      // Don't allow an exception to bubble-up, as this will break operations
      // such as viewing the admin-report status page or invoking
      // `drush queue:list`.
      $this->logger->warning('AWS credentials not accepted.');
    }
    catch (\InvalidArgumentException $e) {
      $this->logger->warning($e->getMessage());
    }
    catch (SqsException $e) {
      $this->logger->warning($e->getAwsErrorMessage());
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function claimItem($lease_time = NULL) {
    if (is_null($lease_time)) {
      $lease_time = $this->leaseTime;
    }

    $response = $this->getClient()->receiveMessage([
      'QueueUrl' => $this->getQueueUrl(),

      // Drupal queues expect a single message at a time.
      'MaxNumberOfMessages' => 1,

      // Include the creation-time in the message response.
      'AttributeNames' => ['SentTimestamp', 'MessageGroupId'],

      // The message will not be available to other consumers until the lease-
      // time is expired, or the item is released.
      // The minimum is 0, the maximum is 43200 (12 hours).
      // Drupal's interface sets a default of 1 hour.
      'VisibilityTimeout' => $lease_time,

      // If there are 0 messages in the queue, wait for up to [x] seconds for a
      // new message to be available.
      'WaitTimeSeconds' => $this->waitTime,
    ]);

    $messages = $response->get('Messages');
    if (!$messages) {
      return FALSE;
    }
    $message = reset($messages);

    // Create a debug-level entry in the log, containing the receipt handle.
    $this->logger->debug('Received message with receipt handle "{receiptId}".', [
      'receiptId' => $message['ReceiptHandle'],
    ]);

    $data = ($this->serializationFormat)
      ? $this->deserialize($message['Body'])
      : $message['Body'];

    $queueItem = new AwsSqsQueueItem(
      $data,
      $message['MessageId'],
      (int) floor($message['Attributes']['SentTimestamp'] / 1000));

    // Provide a reference to the queue so that item can be extended/released.
    $queueItem->setQueue($this);

    // The receipt ID is not part of the general Drupal queue interface, but
    // is important for queue operations on SQS queue items, such as delete
    // and release operations.
    $queueItem->setReceipt($message['ReceiptHandle']);

    // The message group ID only applies to FIFO queues.
    if ($this->isReliable()) {
      $queueItem->setmessageGroupId($message['Attributes']['MessageGroupId']);
    }

    return $queueItem;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItem($item) {
    if (empty($item->receipt)) {
      throw new \Exception('SQS queue item requires a receipt ID.');
    }

    $this->getClient()->deleteMessage([
      'QueueUrl' => $this->getQueueUrl(),
      'ReceiptHandle' => $item->receipt,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function releaseItem($item) {
    if (empty($item->receipt)) {
      throw new \Exception('SQS queue item requires a receipt ID.');
    }

    $this->getClient()->changeMessageVisibility([
      'QueueUrl' => $this->getQueueUrl(),
      'ReceiptHandle' => $item->receipt,
      'VisibilityTimeout' => 0,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createQueue() {
    try {
      $result = $this->getClient()->createQueue([
        'QueueName' => $this->queueConfiguration->aws_queue_name,
        'Attributes' => [
          'VisibilityTimeout' => $this->queueConfiguration->lease_time,
        ],
      ]);

      // Attempting to create a queue soon after removing a queue will often
      // fail.
      // Return TRUE if the result provides the URL of the created queue.
      return (bool) $result->get('QueueUrl');
    }
    catch (AwsException $e) {
      $variables = Error::decodeException($exception);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteQueue() {
    try {
      $this->getClient()->deleteQueue([
        'QueueUrl' => $this->getQueueUrl(),
      ]);
      $this->logger->error('Queue %id deleted.', ['%id' => $this->queueConfiguration->aws_queue_name]);
    }
    catch (AwsException $e) {
      $variables = Error::decodeException($exception);
      $this->logger->error('%type: @message in %function (line %line of %file).', $variables);
    }
  }

}
