<?php

namespace Drupal\aws_sqs_api\Queue;

use function Opis\Closure\serialize as opis_serialize;
use function Opis\Closure\unserialize as opis_unserialize;

/**
 * Configuration for a queue backed by SQS.
 *
 * Several keys are reserved for the Drupal queue configuration:
 * - aws_queue_name
 * - drupal_queue_name
 * - lease_time
 * - wait_time
 * - naming
 * - autocreate.
 *
 * All other keys are treated as arguments for the client constructor.
 */
class Configuration {

  /**
   * An AWS queue name is optional.
   *
   * If an AWS queue name is not specified, it will use the ID provided by the
   * Drupal queue worker plugin.
   *
   * @var string
   */
  const NAME_OPTIONAL = 'optional';

  /**
   * An AWS queue name is required.
   *
   * @var string
   */
  const NAME_REQUIRED = 'required';

  /**
   * The name of the drupal queue worker plugin.
   *
   * @var string
   */
  protected $id;

  /**
   * The definition provided by the queue worker plugin.
   *
   * @var array
   */
  protected $pluginDefinition = [];

  /**
   * Configuration to use when initializing the Drupal queue.
   *
   * @var array
   */
  protected $config = [];

  /**
   * Constructor.
   *
   * @param string $id
   *   The name of the drupal queue worker plugin.
   * @param array $plugin_definition
   *   (optional) The definition provided by the queue worker plugin.
   */
  public function __construct($id, array $plugin_definition = []) {
    $this->id = $id;
    $this->pluginDefinition = $plugin_definition;
  }

  /**
   * Get the name of the drupal queue worker plugin.
   *
   * @return string
   *   The name of the drupal queue worker plugin.
   */
  public function id() {
    return $this->id;
  }

  /**
   * Get the definition provided by the queue worker plugin.
   *
   * @return array
   *   The definition provided by the queue worker plugin.
   */
  public function getPluginDefinition() {
    return $this->pluginDefinition;
  }

  /**
   * Get a configuration entry.
   *
   * @param string $key
   *   The name of the entry, such as 'wait_time'.
   *
   * @return mixed
   *   The value of the entry, such as '10'.
   */
  public function get($key) {
    return $this->config[$key] ?? NULL;
  }

  /**
   * Set a configuration entry.
   *
   * @param string $key
   *   The name of the entry, such as 'wait_time'.
   * @param mixed $value
   *   The value of the entry, such as '10'.
   *
   * @return $this
   *   Fluent interface.
   */
  public function set($key, $value) {
    $this->config[$key] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function __get($key) {
    return $this->config[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function __set($key, $value) {
    $this->config[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function __isset($key) {
    return $this->config[$key] ?? NULL;
  }

  /**
   * Get the arguments to provide to the client constructor.
   *
   * @return array
   *   Array of arguments to pass to the AWS SQS client constructor.
   */
  public function clientConstructorArguments() {
    $reservedKeys = array_fill_keys([
      'aws_queue_name',
      'drupal_queue_name',
      'lease_time',
      'wait_time',
      'naming',
      'autocreate',
    ], NULL);
    return array_diff_key($this->config, $reservedKeys);
  }

  /**
   * {@inheritdoc}
   */
  public function __sleep() {
    $this->dormant = opis_serialize((object) $this->config);
    unset($this->config);
    return array_keys(\get_object_vars($this));
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    $this->config = (array) opis_unserialize($this->dormant);
    unset($this->dormant);
  }

}
