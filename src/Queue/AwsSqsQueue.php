<?php

namespace Drupal\aws_sqs_api\Queue;

use Aws\Exception\CredentialsException;
use Aws\Sqs\Exception\SqsException;
use Aws\Sqs\SqsClient;
use Drupal\Core\Queue\QueueInterface;
use Drupal\serialization\Encoder\JsonEncoder;
use Drupal\serialization\Encoder\XmlEncoder;
use Psr\Log\LoggerInterface;

/**
 * A Drupal Queue implementation which integrates against AWS SQS.
 */
class AwsSqsQueue implements QueueInterface {

  /**
   * The API version may be bumped in future module releases.
   *
   * @var string
   */
  const SQS_CLIENT_VERSION = '2012-11-05';

  /**
   * Select the region 'us-west-1' if a region or other default is not set.
   *
   * @var string
   */
  const DEFAULT_REGION = 'us-west-1';

  /**
   * By default, allow 60 seconds for an item to be processed.
   *
   * @var int
   */
  const DEFAULT_LEASE_TIME = 60;

  /**
   * By default, wait 5 seconds whilst polling for a message to arrive.
   *
   * @var int
   */
  const DEFAULT_WAIT_TIME = 5;

  /**
   * The queue is present and accessible.
   *
   * @var int
   */
  const STATUS_OK = 0;

  /**
   * The AWS credentials are not present or are invalid.
   *
   * @var int
   */
  const STATUS_CREDENTIALS_NOT_ACCEPTED = 1;

  /**
   * The queue does not have an AWS SQS queue name configured.
   *
   * @var int
   */
  const STATUS_QUEUE_NAME_NOT_DEFINED = 2;

  /**
   * The queue was not found in the specified region.
   */
  const STATUS_QUEUE_NOT_PRESENT = 3;

  /**
   * An unknown error occurred.
   */
  const STATUS_UNKNOWN_ERROR = 100;

  // This trait provides the concrete methods required by QueueInterface.
  use DrupalQueueInterfaceImplementationTrait;

  /**
   * Configuration for this queue implementation.
   *
   * @var \Drupal\aws_sqs_api\Queue\Configuration
   */
  protected $queueConfiguration;

  /**
   * The attributes of the queue, return by the client service.
   *
   * This is lazy-loaded by an invocation to `attr()`.
   *
   * @var array
   */
  protected $sqsQueueAttributes;

  /**
   * Drupal watchdog logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Serializer services.
   *
   * Array with the keys 'xml' and 'json'.
   *
   * @var array
   */
  protected $serializer;

  /**
   * Serialize messages to JSON by default.
   *
   * Set this to NULL for no serialization of data.
   *
   * @var Enum[json|xml|null]
   */
  protected $serializationFormat = 'json';

  /**
   * Create a Drupal queue class to integrate with AWS SQS.
   *
   * @param \Drupal\aws_sqs_api\Queue\Configuration $config
   *   The configuration properties for this queue.
   */
  public function __construct(Configuration $config) {
    $this->queueConfiguration = $config;
  }

  /**
   * Get the ID of this queue.
   *
   * @return string
   *   The machine name of the Drupal queue-worker implemented for this queue.
   */
  public function id() {
    return $this->queueConfiguration->id();
  }

  /**
   * Get the human-readable label for this queue.
   *
   * @return string
   *   The label.
   */
  public function label() {
    return $this->queueConfiguration->drupal_queue_name;
  }

  /**
   * Purge all messages from the queue.
   *
   * @return bool|string
   *   TRUE is returned if the purge operation was successful. If the operation
   *   failed, the AWS error message is returned.
   */
  public function purge() {
    try {
      $this->getClient()->purgeQueue([
        'QueueUrl' => $this->getQueueUrl(),
      ]);
      $this->logger->notice('Queue {id} purged.', [
        'id' => $this->config->drupalQueueName,
      ]);
      return TRUE;
    }
    catch (SqsException $e) {
      switch ($e->getAwsErrorCode()) {
        case 'AWS.SimpleQueueService.PurgeQueueInProgress':
          $this->logger->warning($e->getAwsErrorMessage());
          break;

        case 'AWS.SimpleQueueService.NonExistentQueue':
          $this->logger->error($e->getAwsErrorMessage());
          break;

        default:
          $this->logger->error('An unknown error occurred: {message}.', [
            'message' => $e->getAwsErrorMessage(),
          ]);
      }
      return $e->getAwsErrorMessage();
    }
  }

  /**
   * Set the serializer service for JSON.
   *
   * @param \Drupal\serialization\Encoder\JsonEncoder $serializer
   *   The JSON serializer service.
   */
  public function setSerializerJson(JsonEncoder $serializer) {
    $this->serializer['json'] = $serializer;
  }

  /**
   * Set the serializer service for XML.
   *
   * @param \Drupal\serialization\Encoder\XmlEncoder $serializer
   *   The XML serializer service.
   */
  public function setSerializerXml(XmlEncoder $serializer) {
    $this->serializer['xml'] = $serializer;
  }

  /**
   * Set the logger service.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   */
  public function setLogger(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * Set the serialization format.
   *
   * Accepted values are json (default), xml, or null.
   * Set to NULL to disable serialization (and only plain text will be
   * accepted).
   *
   * @param string|null $format
   *   Serialization format. Must be one of: "json", "xml", or NULL.
   */
  public function setSerializationFormat($format) {
    if (is_string($format) && !($format === 'json' || $format === 'xml')) {
      throw new \Exception('Invalid message format.');
    }
    elseif (!is_null($format)) {
      throw new \Exception('Invalid message format.');
    }
    $this->serializationFormat = $format;
  }

  /**
   * Retrieve a queue attribute from SQS.
   *
   * This will lazy-load the attribute collection if it's not already set.
   *
   * @param string $attribute
   *   The name of the attribute to retrieve.
   *
   * @return mixed
   *   The value of the attribute returned by the SQS service.
   */
  public function attr($attribute) {
    if (empty($this->sqsQueueAttributes)) {
      $this->gatherAttributes();
    }
    if (array_key_exists($attribute, $this->sqsQueueAttributes)) {
      return $this->sqsQueueAttributes[$attribute];
    }
  }

  /**
   * Determine whether the queue is configured and can connect.
   *
   * @return int
   *   Whether the queue is configured and reachable. One of:
   *   - self::STATUS_OK
   *   - self::STATUS_CREDENTIALS_NOT_ACCEPTED
   *   - self::STATUS_QUEUE_NAME_NOT_DEFINED
   *   - self::STATUS_QUEUE_NOT_PRESENT
   */
  public function status() {
    if (empty($this->queueConfiguration->aws_queue_name)) {
      return self::STATUS_QUEUE_NAME_NOT_DEFINED;
    }
    try {
      $queueUrl = $this->getQueueUrl();

      // Attempt to create the queue if it doesn't exist.
      if (!$queueUrl) {
        if ($this->queueConfiguration->naming === CONFIGURATION::NAME_OPTIONAL
          && $this->queueConfiguration->autocreate) {
          $success = $this->createQueue();
          if ($success) {
            return self::STATUS_OK;
          }
          else {
            return self::STATUS_UNKNOWN_ERROR;
          }
        }
        else {
          return self::STATUS_QUEUE_NAME_NOT_DEFINED;
        }
      }
      return self::STATUS_OK;
    }
    catch (CredentialsException $e) {
      return self::STATUS_CREDENTIALS_NOT_ACCEPTED;
    }
    catch (\InvalidArgumentException $e) {
      return self::STATUS_QUEUE_NAME_NOT_DEFINED;
    }
    catch (SqsException $e) {
      return self::STATUS_QUEUE_NOT_PRESENT;
    }
  }

  /**
   * Get a configuration property.
   *
   * @return mixed
   *   The configuration property specified by the key, if it exists.
   */
  public function get($key) {
    return $this->queueConfiguration->$key;
  }

  /**
   * Get the configuration object.
   *
   * @return \Drupal\aws_sqs_api\Queue\Configuration
   *   The configuration object.
   */
  public function getConfiguration() {
    return $this->queueConfiguration;
  }

  /**
   * Check whether the queue is a reliable FIFO queue or a standard queue.
   *
   * @return bool
   *   TRUE if the queue is a reliable FIFO queue.
   */
  protected function isReliable() {
    return preg_match('/\.fifo$/', $this->getQueueUrl());
  }

  /**
   * Serialize data to the format defined for the queue (XML or JSON).
   *
   * @param mixed $data
   *   The queue item payload to create.
   *
   * @return string
   *   The queue item payload serialized into XML or JSON format.
   */
  protected function serialize($data) {
    if (!empty($this->serializer[$this->serializationFormat])) {
      return $this->serializer[$this->serializationFormat]
        ->encode($data);
    }
  }

  /**
   * Unserialize data from the format defined for the queue (XML or JSON).
   *
   * @param mixed $data
   *   The queue item payload to create.
   *
   * @return mixed
   *   The queue item payload deserialized into its original format.
   */
  protected function unserialize($data) {
    if (!empty($this->serializer[$this->serializationFormat])) {
      return $this->serializer[$this->serializationFormat]
        ->decode($data);
    }
  }

  /**
   * Fetch the AWS URL of the SQS Queue.
   *
   * @return string
   *   The URL of the SQS queue.
   */
  protected function getQueueUrl() {
    return $this->getClient()
      ->getQueueUrl([
        'QueueName' => $this->queueConfiguration->aws_queue_name,
      ])
      ->get('QueueUrl');
  }

  /**
   * Query the SQS client to populate the attributes property.
   *
   * This populates the property $this->sqsQueueAttributes and does not return
   * a value.
   */
  protected function gatherAttributes() {
    if (empty($this->sqsQueueAttributes)) {
      $this->sqsQueueAttributes = $this->getClient()->getQueueAttributes([
        'AttributeNames' => ['All'],
        'QueueUrl' => $this->getQueueUrl(),
      ])->get('Attributes');
    }
  }

  /**
   * Provide a default method for generating a deduplication ID.
   *
   * @param mixed $data
   *   The data to add to the queue (before serialization).
   *
   * @return string
   *   A unique ID to use as the message deduplication ID.
   */
  protected static function reliableQueueMessageDeduplicationId($data) {
    // Use an MD5 hash of the message, and the timestamp, to generate the ID.
    // Note that `time()` is deliberately used instead of the request time,
    // to ensure that seemingly-identical messages sent within a long-running
    // process are treated as different messages.
    return md5(serialize($data) . time());
  }

  /**
   * Provide a default method for generating a message group ID.
   *
   * @see https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/using-messagegroupid-property.html
   *
   * "The message group ID is the tag that specifies that a message belongs to
   *  a specific message group. Messages that belong to the same message group
   *  are always processed one by one, in a strict order relative to the
   *  message group (however, messages that belong to different message groups
   *  might be processed out of order)."
   *
   * @return string
   *   A group ID which uses the website hostname to group messages.
   */
  protected static function reliableQueueMessageGroupId() {
    // Use the site's domain name as the group ID.
    return parse_url($GLOBALS['base_url'], PHP_URL_HOST);
  }

  /**
   * Get the AWS SQS client.
   *
   * @return \Aws\SqsClient
   *   An AWS SQS client.
   */
  protected function getClient() {
    static $client = NULL;
    if (!$client) {
      $args = $this->queueConfiguration->clientConstructorArguments();
      // Lock the API version argument.
      $args['version'] = self::SQS_CLIENT_VERSION;
      $client = new SqsClient($args);
    }
    return $client;
  }

}
