<?php

namespace Drupal\aws_sqs_api\Event;

use Drupal\aws_sqs_api\Queue\Configuration;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Autodiscover configuration for an SQS-backed Drupal queue.
 *
 * Precedent order is:
 * - Global defaults ('aws_sqs_api.default_settings')
 * - Plugin definition
 * - Queue configuration
 * - Any other overrides.
 *
 * Discoverable QUEUE properties are:
 * - SQS queue name
 * - Lease time
 * - Wait time.
 *
 * Discoverable CLIENT properties are:
 * - Region
 * - Any other client constructor arguments.
 *   These may include credentials, proxies, and other arbitrary properties.
 */
class SqsQueueConfigEvent extends Event {

  /**
   * Perform discovery of the SQS configuration for a specific Drupal queue.
   *
   * @var string
   */
  const EVENT_KEY = 'aws_sqs_api.configure_queue';

  /**
   * Queue configuration definition.
   *
   * @var \Drupal\aws_sqs_api\Queue\Configuration
   */
  protected $config;

  /**
   * Set the configuration definition.
   *
   * @param \Drupal\aws_sqs_api\Queue\Configuration $config
   *   The queue configuration definition.
   */
  public function setConfiguration(Configuration $config) {
    $this->config = $config;
  }

  /**
   * Get the configuration definition.
   *
   * @return \Drupal\aws_sqs_api\Queue\Configuration
   *   The queue configuration definition.
   */
  public function getConfiguration() {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function __set($key, $value) {
    $this->config->set($key, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function __get($key) {
    return $this->config->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function __isset($key) {
    return !empty($this->config->get($key));
  }

}
