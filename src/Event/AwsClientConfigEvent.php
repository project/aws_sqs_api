<?php

namespace Drupal\aws_sqs_api\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Autodiscover configuration for an AWS EC2 or SQS client.
 */
class AwsClientConfigEvent extends Event {

  /**
   * Perform discovery of the SQS client configuration.
   *
   * @var string
   */
  const SQS_CLIENT = 'aws_sqs_api.configure_sqs_client';

  /**
   * Perform discovery of the EC2 client configuration.
   *
   * @var string
   */
  const EC2_CLIENT = 'aws_sqs_api.configure_ec2_client';

  /**
   * Configuration for an AWS client.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Set the configuration.
   *
   * @param array $configuration
   *   Configuration for an AWS client.
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration;
  }

  /**
   * Get the configuration.
   *
   * @return array
   *   Configuration for an AWS client.
   */
  public function getConfiguration() {
    return $this->configuration;
  }

}
