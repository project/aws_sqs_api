<?php

namespace Drupal\aws_sqs_api\Routing\ParamConverter;

use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\Core\Queue\QueueFactory;
use Symfony\Component\Routing\Route;

/**
 * Upcast a Drupal queue name to an SQS queue definition.
 */
class QueueConverter implements ParamConverterInterface {

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructs a new paramater upcaster for queue IDs.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory service..
   */
  public function __construct(QueueFactory $queue_factory) {
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    if (!empty($value) && is_string($value)) {
      /** @var \Drupal\aws_sqs_api\Queue\AwsSqsQueue */
      if ($queue = $this->queueFactory->get($value)) {
        if ($queue instanceof AwsSqsQueue) {
          return $queue;
        }
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return !empty($definition['type']) && $definition['type'] == 'queue';
  }

}
