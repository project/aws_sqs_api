<?php

namespace Drupal\aws_sqs_api\Controller;

use Drupal\aws_sqs_api\Form\ConfigureQueueForm;
use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\aws_sqs_api\Service\DrupalSqsQueueInformation;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An admin page which lists the SQS queues and links to configuration.
 */
class QueueList extends ControllerBase {

  /**
   * The Drupal SQS queue information service.
   *
   * @var \Drupal\aws_sqs_api\Service\DrupalSqsQueueInformation
   */
  protected $sqsQueueInformation;

  /**
   * The queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\aws_sqs_api\Service\DrupalSqsQueueInformation $info_svc
   *   The AWS information service.
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue factory service.
   */
  public function __construct(DrupalSqsQueueInformation $info_svc, QueueFactory $queue_factory) {
    $this->sqsQueueInformation = $info_svc;
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_sqs_api.drupal_queue_information'),
      $container->get('queue')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function content() {
    $tableHeaders = [
      'drupal_queue_name' => $this->t('Drupal queue'),
      'aws_queue_name'    => $this->t('SQS queue name'),
      'region'            => $this->t('Region'),
      'count_in_queue'    => $this->t('Messages queued'),
      'count_in_flight'   => $this->t('Messages in-flight'),
      'operations'        => $this->t('Operations'),
    ];

    $build['queues'] = [
      '#attributes' => [
        'class' => [
          'aws_sqs_api_queue_list',
        ],
      ],
      '#attached' => [
        'library' => [
          'aws_sqs_api/admin',
        ],
      ],
      '#type' => 'table',
      '#header' => $tableHeaders,
      '#empty' => $this->t('No queues defined to use SQS.'),
    ];

    // Each $config item as an instance of
    // Drupal\aws_sqs_api\Queue\Configuration.
    foreach ($this->sqsQueueInformation->getDefinitions() as $key => $config) {
      $queue = $this->queueFactory->get($key);

      $queueClasses = [];
      $queueClasses[] = ($queue->status() === $queue::STATUS_OK) ? 'configured' : 'unconfigured';

      $row = [
        '#attributes' => [
          'class' => $queueClasses,
        ],
        'drupal_queue_name' => [
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#value' => $config->get('drupal_queue_name'),
          '#attributes' => [
            'title' => $key,
          ],
        ],
        'aws_queue_name' => [
          '#plain_text' => $config->get('aws_queue_name'),
        ],
        'region' => [
          '#plain_text' => $config->get('region'),
        ],
        'count_in_queue' => [
          '#plain_text' => ($queue->status() === 0) ? $queue->attr('ApproximateNumberOfMessages') : '-',
        ],
        'count_in_flight' => [
          '#plain_text' => ($queue->status() === 0) ? $queue->attr('ApproximateNumberOfMessagesNotVisible') : '-',
        ],
        'operations' => [
          'data' => [
            '#type' => 'operations',
            '#links' => $this->operations($queue),
          ],
        ],
      ];
      $build['queues'][] = $row;
    }

    return $build;
  }

  /**
   * List the available operations for a given queue.
   *
   * Operations are:
   * - Override/Add/Edit configuration for:
   *   Region, queue name, wait time and lease time.
   * - Purge queue.
   *
   * @todo Add 'Revert overrides' option.
   *
   * @param \Drupal\aws_sqs_api\Queue\AwsSqsQueue $queue
   *   The queue.
   *
   * @return array
   *   An array of links for an operation render element.
   */
  protected function operations(AwsSqsQueue $queue) {
    $configKey = ConfigureQueueForm::CONFIG_KEY_PREFIX . '.' . $queue->id();
    $config = $this->config($configKey);

    $pluginDefaults = (array_key_exists('aws_sqs', $queue->getConfiguration()->getPluginDefinition()))
      ? $queue->getConfiguration()->getPluginDefinition()['aws_sqs']
      : [];

    if ($config->get()) {
      $text = $this->t('Edit configuration');
    }
    elseif ($pluginDefaults) {
      $text = $this->t('Override configuration');
    }
    else {
      $text = $this->t('Configure');
    }

    $ops = [];
    $ops['edit'] = [
      'title' => $text,
      'url' => Url::fromRoute('aws_sqs_api.configure_queue_form', ['queue' => $queue->id()]),
    ];

    if ($queue->status() === $queue::STATUS_OK) {
      $ops['purge'] = [
        'title' => $this->t('Purge queue'),
        'url' => Url::fromRoute('aws_sqs_api.purge_queue_confirm_form', ['queue' => $queue->id()]),
      ];
    }

    return $ops;
  }

}
