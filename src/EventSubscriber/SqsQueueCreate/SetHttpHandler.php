<?php

namespace Drupal\aws_sqs_api\EventSubscriber\SqsQueueCreate;

use Drupal\aws_sqs_api\Event\SqsQueueConfigEvent;
use GuzzleHttp\ClientInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Attach the Drupal HTTP client handler to the SQS client.
 */
class SetHttpHandler implements EventSubscriberInterface {

  /**
   * The http client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Create an event subscriber to attach the HTTP client to the queue.
   *
   * This ensures the SQS requests are handled by Drupal's HTTP client, so that
   * any client middleware (proxies/logging/etc) are applied.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client service.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SqsQueueConfigEvent::EVENT_KEY => ['configure', 1],
    ];
  }

  /**
   * Attach the HTTP client handler.
   *
   * @param \Drupal\aws_sqs_api\Event\SqsQueueConfigEvent $event
   *   The SQS queue config auto-discovery event.
   */
  public function configure(SqsQueueConfigEvent $event) {
    $event->getConfiguration()->set('http_handler', $this->httpClient->getConfig('handler'));
  }

}
