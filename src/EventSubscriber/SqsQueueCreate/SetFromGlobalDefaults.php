<?php

namespace Drupal\aws_sqs_api\EventSubscriber\SqsQueueCreate;

use Drupal\aws_sqs_api\Event\SqsQueueConfigEvent;
use Drupal\aws_sqs_api\Form\GlobalDefaultsConfigurationForm;
use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\aws_sqs_api\Queue\Configuration;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Configure the queue with global defaults.
 */
class SetFromGlobalDefaults implements EventSubscriberInterface {

  /**
   * The default config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $defaultConfig;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->defaultConfig = $config_factory->get(GlobalDefaultsConfigurationForm::CONFIG_KEY);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Prioritisation order:
    // - Global defaults: 10
    // - Plugin:           5
    // - Queue config:     1.
    return [
      SqsQueueConfigEvent::EVENT_KEY => ['configure', 10],
    ];
  }

  /**
   * Set the configs according to the global defaults.
   *
   * @param \Drupal\aws_sqs_api\Event\SqsQueueConfigEvent $event
   *   The SQS queue config auto-discovery event.
   */
  public function configure(SqsQueueConfigEvent $event) {
    $keys = [
      'region'     => AwsSqsQueue::DEFAULT_REGION,
      'lease_time' => AwsSqsQueue::DEFAULT_LEASE_TIME,
      'wait_time'  => AwsSqsQueue::DEFAULT_WAIT_TIME,
      'naming'     => Configuration::NAME_REQUIRED,
      'autocreate' => FALSE,
    ];
    foreach ($keys as $key => $default) {
      if ($value = $this->defaultConfig->get($key)) {
        $event->$key = $value;
      }
      else {
        $event->$key = $default;
      }
    }

    // If SQS queue names are optional, populate a default AWS queue name.
    if ($this->defaultConfig->get('naming') === Configuration::NAME_OPTIONAL) {
      $event->aws_queue_name = $event->getConfiguration()->id();
    }
  }

}
