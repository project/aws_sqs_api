<?php

namespace Drupal\aws_sqs_api\EventSubscriber\SqsQueueCreate;

use Drupal\aws_sqs_api\Event\SqsQueueConfigEvent;
use Drupal\aws_sqs_api\Form\ConfigureQueueForm;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Configure the queue from queue-specific config.
 */
class SetFromQueueConfig implements EventSubscriberInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactory $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Prioritisation order:
    // - Global defaults: 10
    // - Plugin:           5
    // - Queue config:     1.
    return [
      SqsQueueConfigEvent::EVENT_KEY => ['configure', 1],
    ];
  }

  /**
   * Set the configs according to the global defaults.
   *
   * @param \Drupal\aws_sqs_api\Event\SqsQueueConfigEvent $event
   *   The SQS queue config auto-discovery event.
   */
  public function configure(SqsQueueConfigEvent $event) {
    $queueConfig = $event->getConfiguration();
    $config_key = ConfigureQueueForm::CONFIG_KEY_PREFIX . '.' . $event->getConfiguration()->id();
    if ($config = $this->configFactory->get($config_key)) {
      $keys = [
        'region',
        'aws_queue_name',
        'lease_time',
        'wait_time',
      ];
      foreach ($keys as $key) {
        if ($value = $config->get($key)) {
          $queueConfig->set($key, $value);
        }
      }
    }
  }

}
