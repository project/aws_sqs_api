<?php

namespace Drupal\aws_sqs_api\EventSubscriber\SqsQueueCreate;

use Drupal\aws_sqs_api\Event\SqsQueueConfigEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Configure the queue from a queue-worker plugin definition.
 */
class SetFromPlugin implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // Prioritisation order:
    // - Global defaults: 10
    // - Plugin:           5
    // - Queue config:     1.
    return [
      SqsQueueConfigEvent::EVENT_KEY => ['configure', 5],
    ];
  }

  /**
   * Constructor.
   *
   * @param \Drupal\aws_sqs_api\Event\SqsQueueConfigEvent $event
   *   The SQS queue config auto-discovery event.
   */
  public function configure(SqsQueueConfigEvent $event) {
    if ($definition = $event->getConfiguration()->getPluginDefinition()) {
      $event->drupal_queue_name = $definition['title'];

      if (array_key_exists('aws_sqs', $definition)) {
        foreach ($definition['aws_sqs'] as $key => $value) {
          $event->$key = $value;
        }
      }
    }
  }

}
