<?php

namespace Drupal\aws_sqs_api\Service;

use Drupal\aws_sqs_api\Event\SqsQueueConfigEvent;
use Drupal\aws_sqs_api\Queue\Configuration;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\Core\Site\Settings;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Provide information about Drupal SQS queues.
 */
class DrupalSqsQueueInformation {

  /**
   * The settings object.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * The queue worker manager service.
   *
   * This is used to fetch the queue worker plugins and definitions.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The Drupal settings service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   Queue worker manager service, to load the plugin definitions.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The Symfony event dispatcher.
   */
  public function __construct(Settings $settings, QueueWorkerManagerInterface $queue_worker_manager, EventDispatcherInterface $dispatcher) {
    $this->settings = $settings;
    $this->queueWorkerManager = $queue_worker_manager;
    $this->eventDispatcher = $dispatcher;
  }

  /**
   * Get the definitions of all SQS-implementing Drupal queues.
   */
  public function getDefinitions() {
    $queues = $this->listQueues();
    return array_combine($queues, array_map(function ($name) {
      return $this->getDefinition($name);
    }, $queues));
  }

  /**
   * Get the config definition for a particular queue.
   *
   * @return \Drupal\aws_sqs_api\Queue\Configuration
   *   A queue configuration definition.
   */
  public function getDefinition($name) {
    $config = new Configuration($name, $this->doGetPluginDefinition($name));

    // Pass the results through the event dispatcher to populate the queue
    // configuration and client constructor arguments.
    $event = new SqsQueueConfigEvent();
    $event->setConfiguration($config);
    $this->eventDispatcher->dispatch($event, SqsQueueConfigEvent::EVENT_KEY);
    $config = $event->getConfiguration();

    return $config;
  }

  /**
   * List the queues which are implemented by the AWS SQS API.
   *
   * @return array
   *   A list of the queue names of Drupal queues implementing the SQS API.
   */
  public function listQueues() {
    return array_filter($this->listAllDrupalQueues(),
      ['self', 'isDrupalSqsQueue']);
  }

  /**
   * List all the Drupal queues regardless of implementation.
   *
   * @return array
   *   A list of queue names.
   */
  public function listAllDrupalQueues() {
    return array_keys($this->doGetPluginDefinition());
  }

  /**
   * Test whether a Drupal queue is using this module's SQS implentation.
   *
   * @param string $name
   *   The machine-name of the Drupal queue.
   *
   * @return bool
   *   TRUE if the queue uses this module's SQS implementation.
   */
  protected function isDrupalSqsQueue($name) {
    return $this->getServiceForQueue($name) === 'queue.aws_sqs';
  }

  /**
   * Get the container service name for a given queue.
   *
   * @param string $name
   *   The machine-name of the Drupal queue.
   *
   * @return string
   *   The name of the factory service used to create the queue.
   */
  protected function getServiceForQueue($name) {
    // Use the first available service configured in settings.
    $candidates = [
      'queue_reliable_service_' . $name,
      'queue_service_' . $name,
      'queue_default',
    ];

    foreach ($candidates as $candidate) {
      if ($service = $this->settings->get($candidate)) {
        return $service;
      }
    }

    // Final fallback is the default database queue.
    return 'queue.database';
  }

  /**
   * Get the plugin definition for one or all queue worker plugins.
   *
   * @param string $name
   *   The machine-name of the Drupal queue.
   *
   * @return array
   *   An array of all definitions, or the definition for a particular queue.
   */
  protected function doGetPluginDefinition($name = NULL) {
    static $definitions = NULL;
    if (is_null($definitions)) {
      $definitions = $this->queueWorkerManager->getDefinitions();
    }
    return ($name) ? $definitions[$name] : $definitions;
  }

}
