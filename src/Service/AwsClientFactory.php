<?php

namespace Drupal\aws_sqs_api\Service;

use Aws\Ec2\Ec2Client;
use Aws\Sqs\SqsClient;
use Drupal\aws_sqs_api\Event\AwsClientConfigEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Factory for creating AWS clients.
 */
class AwsClientFactory {

  /**
   * Default region, where this has not been specified or configured.
   *
   * @var string
   */
  const DEFAULT_REGION = 'us-west-1';

  /**
   * API version when using the EC2 client.
   *
   * @var string
   */
  const EC2_CLIENT_VERSION = '2016-11-15';

  /**
   * API version when using the SQS client.
   *
   * @var string
   */
  const SQS_CLIENT_VERSION = '2012-11-05';

  /**
   * Constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The Symfony event dispatcher.
   */
  public function __construct(EventDispatcherInterface $dispatcher) {
    $this->eventDispatcher = $dispatcher;
  }

  /**
   * Create an EC2 client.
   *
   * @param array $args
   *   (optional) Default arguments to pass to the Ec2Client constructor.
   *   Arguments may be altered by the configuration event.
   *
   * @return \Aws\Ec2\Ec2Client
   *   The EC2 client.
   */
  public function ec2Client(array $args = []) {
    return new Ec2Client($this->configureEc2Client($args));
  }

  /**
   * Create an SQS client.
   *
   * @param array $args
   *   (optional) Default arguments to pass to the SqsClient constructor.
   *   Arguments may be altered by the configuration event.
   *
   * @return \Aws\Sqs\SqsClient
   *   The SQS client.
   */
  public function sqsClient(array $args = []) {
    return new SqsClient($this->configureSqsClient($args));
  }

  /**
   * Fetch the configuration to use when constructing an Ec2Client.
   *
   * @param array $default_args
   *   The initial set of arguments.
   *
   * @return array
   *   The set of arguments populated after configuration.
   */
  public function configureEc2Client(array $default_args) {
    // Enforce the API versions.
    $default_args['version'] = self::EC2_CLIENT_VERSION;

    $event = new AwsClientConfigEvent();
    $event->setConfiguration($default_args);
    $this->eventDispatcher->dispatch($event, AwsClientConfigEvent::EC2_CLIENT);
    $args = $event->getConfiguration();
    if (empty($args['region'])) {
      $args['region'] = self::DEFAULT_REGION;
    }
    return $args;
  }

  /**
   * Fetch the configuration to use when constructing an SqsClient.
   *
   * @param array $default_args
   *   The initial set of arguments.
   *
   * @return array
   *   The set of arguments populated after configuration.
   */
  public function configureSqsClient(array $default_args) {
    // Enforce the API versions.
    $default_args['version'] = self::SQS_CLIENT_VERSION;

    $event = new AwsClientConfigEvent();
    $event->setConfiguration($default_args);
    $this->eventDispatcher->dispatch($event, AwsClientConfigEvent::SQS_CLIENT);
    $args = $event->getConfiguration();
    if (empty($args['region'])) {
      $args['region'] = self::DEFAULT_REGION;
    }
    return $args;
  }

}
