<?php

namespace Drupal\aws_sqs_api\Service;

use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\serialization\Encoder\JsonEncoder;
use Drupal\serialization\Encoder\XmlEncoder;

/**
 * Create an AWS SQS Queue operator.
 */
class AwsSqsQueueFactory {

  /**
   * Drupal queue information service.
   *
   * @var \Drupal\aws_sqs_api\Service\DrupalSqsQueueInformation
   */
  protected $drupalQueueInfoService;

  /**
   * Drupal watchdog logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Drupal JSON serializer service.
   *
   * @var \Drupal\serialization\Encoder\JsonEncoder
   */
  protected $serializerJson;

  /**
   * Drupal XML serializer service.
   *
   * @var \Drupal\serialization\Encoder\XmlEncoder
   */
  protected $serializerXml;

  /**
   * Constructor.
   *
   * @param \Drupal\aws_sqs_api\Service\DrupalSqsQueueInformation $queue_info
   *   Queue information service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory.
   * @param \Drupal\serialization\Encoder\JsonEncoder $serializer_json
   *   Serializer service for JSON.
   * @param \Drupal\serialization\Encoder\XmlEncoder $serializer_xml
   *   Serializer service for XML.
   */
  public function __construct(
    DrupalSqsQueueInformation $queue_info,
    LoggerChannelFactory $logger_factory,
    JsonEncoder $serializer_json,
    XmlEncoder $serializer_xml
  ) {
    $this->drupalQueueInfoService = $queue_info;
    $this->logger = $logger_factory->get('AWS SQS API');
    $this->serializerJson = $serializer_json;
    $this->serializerXml = $serializer_xml;
  }

  /**
   * Constructs a new queue object for a given name.
   *
   * @param string $name
   *   The ID of the Drupal Queue Worker plugin.
   *
   * @return \Drupal\aws_sqs_api\AwsSqsQueue
   *   Return an AwsSqsQueue object.
   */
  public function get($name) {
    /** @var \Drupal\aws_sqs_api\Queue\Configuration */
    $configuration = $this->drupalQueueInfoService->getDefinition($name);

    $queue = new AwsSqsQueue($configuration);

    $queue->setLogger($this->logger);
    $queue->setSerializerJson($this->serializerJson);
    $queue->setSerializerXml($this->serializerXml);

    return $queue;
  }

}
