<?php

namespace Drupal\aws_sqs_api\Service;

/**
 * Retrieve information from AWS about available regions and queues.
 */
class AwsInformation {

  /**
   * AWS client factory service.
   *
   * @var \Drupal\aws_sqs_api\Service\AwsClientFactory
   */
  protected $clientFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\aws_sqs_api\Service\AwsClientFactory $client_factory
   *   The client factory service.
   */
  public function __construct(AwsClientFactory $client_factory) {
    $this->clientFactory = $client_factory;
  }

  /**
   * Fetch a list of queues available in a region.
   *
   * @param string $region
   *   (optional) AWS region, in the format 'us-west-1'. If this is not
   *   specified, a default region will be selected from configuration.
   *
   * @return array
   *   An array of queue names.
   */
  public function listQueues($region = NULL) {
    $args = $region ? ['region' => $region] : [];
    $client = $this->clientFactory->sqsClient($args);

    /** @var \Aws\Result */
    $result = $client->listQueues([]);

    if ($urls = $result->get('QueueUrls')) {
      return array_map(function ($queueUrl) {
        return basename($queueUrl);
      }, $urls);
    }
    return [];
  }

  /**
   * Fetch a list of regions enabled.
   *
   * @return array
   *   An array of region names.
   */
  public function listRegions() {
    return self::regionListFallback();
  }

  /**
   * Fallback to provide a list of regions, if this can't be fetched from AWS.
   *
   * @return array
   *   An alphabetically-ordered list of AWS region-names.
   */
  protected static function regionListFallback() {
    return [
      'ap-northeast-1',
      'ap-northeast-2',
      'ap-northeast-3',
      'ap-south-1',
      'ap-southeast-1',
      'ap-southeast-2',
      'ca-central-1',
      'eu-central-1',
      'eu-north-1',
      'eu-west-1',
      'eu-west-2',
      'eu-west-3',
      'sa-east-1',
      'us-east-1',
      'us-east-2',
      'us-west-1',
      'us-west-2',
    ];
  }

}
