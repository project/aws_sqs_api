<?php

namespace Drupal\aws_sqs_api\Form;

/**
 * Provide shared select-field options for forms.
 */
trait FormOptionsTrait {

  /**
   * Fetch a list of regions, including their number of queues.
   */
  protected function regionOptions() {
    $regions = [];
    foreach ($this->awsInformation->listRegions() as $region) {
      $count = count($this->awsInformation->listQueues($region));
      $regions[$region] = $this->t('@region (@count)', [
        '@region' => $region,
        '@count' => $count,
      ]);
    }
    return $regions;
  }

  /**
   * Fetch a set of options for default lease time (in seconds).
   *
   * @return array
   *   Array indexed by time (in seconds) and a human-readable description.
   */
  protected function leaseTimeOptions() {
    return [
      1     => $this->t('1 second'),
      5     => $this->t('5 seconds'),
      10    => $this->t('10 seconds'),
      15    => $this->t('15 seconds'),
      20    => $this->t('20 seconds'),
      30    => $this->t('30 seconds'),
      60    => $this->t('1 minute'),
      120   => $this->t('2 minutes'),
      300   => $this->t('5 minutes'),
      600   => $this->t('10 minutes'),
      1800  => $this->t('30 minutes'),
      3600  => $this->t('1 hour'),
      7200  => $this->t('2 hours'),
      10800 => $this->t('3 hours'),
      21600 => $this->t('6 hours'),
      43200 => $this->t('12 hours (maximum)'),
    ];
  }

  /**
   * Fetch a set of options for default wait time (in seconds).
   *
   * @return array
   *   Array indexed by time (in seconds) and a human-readable description.
   */
  protected function waitTimeOptions() {
    $options = [];
    for ($i = 0; $i <= 30; $i++) {
      $options[$i] = $this->formatPlural($i, '@count second', '@count seconds');
    }
    return $options;
  }

}
