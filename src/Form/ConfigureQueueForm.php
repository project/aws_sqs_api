<?php

namespace Drupal\aws_sqs_api\Form;

use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\aws_sqs_api\Queue\Configuration;
use Drupal\aws_sqs_api\Service\AwsInformation;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure a Drupal queue with SQS-specific settings.
 */
class ConfigureQueueForm extends ConfigFormBase {

  use FormOptionsTrait;

  /**
   * Config key prefix where each queue's config will be stored.
   *
   * @var string
   */
  const CONFIG_KEY_PREFIX = 'aws_sqs_api.queue_configuration';

  /**
   * AWS information service.
   *
   * This is used to query a list of regions.
   *
   * @var \Drupal\aws_sqs_api\Service\AwsInformation
   */
  protected $awsInformation;

  /**
   * Construct a configuration form for SQS defaults.
   *
   * @param \Drupal\aws_sqs_api\Service\AwsInformation $aws_information
   *   AWS information service.
   */
  public function __construct(AwsInformation $aws_information) {
    $this->awsInformation = $aws_information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_sqs_api.aws_information')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_sqs_api.configure_queue_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_KEY_PREFIX . '.' . $this->queue->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AwsSqsQueue $queue = NULL) {
    $this->queue = $queue;
    $config_key = reset($this->getEditableConfigNames());
    $config = $this->config($config_key);

    $form['#attached'] = [
      'library' => [
        'aws_sqs_api/admin',
      ],
    ];

    $form['drupal_queue_name'] = [
      '#type' => 'markup',
      '#markup' => '<h2>' . $queue->label() . '</h2>',
    ];

    $form['info'] = [];
    $form['info']['config_key'] = [
      '#type' => 'item',
      '#title' => $this->t('Configuration key'),
      '#markup' => '<pre>' . $config_key . '</pre>',
    ];
    $form['region'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Region'),
    ];
    $regions = $this->regionOptions();
    $form['region']['region'] = [
      '#type' => 'select',
      '#title' => $this->t('AWS region'),
      '#options' => $regions,
      '#default_value' => $config->get('region') ?? $queue->get('region'),

      // Dynamically refresh the list of queues when the region is changed.
      '#ajax' => [
        'callback' => '::ajaxRefreshQueues',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'select-queue',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Updating queue list…'),
        ],
      ],
    ];
    $form['region']['refresh'] = [
      '#type' => 'submit',
      '#executes_submit_callback' => FALSE,
      '#limit_validation_errors' => [
        ['region'],
      ],
      '#value' => $this->t('Update queue list'),

      // Using the form '#states' property ensures this is hidden when
      // JavaScript is available and operating as expected (in which case, a
      // change to the selected region will auto-refresh the queue list).
      '#states' => [
        'invisible' => [
          ':input[name="region"]' => ['empty' => FALSE],
        ],
      ],
    ];

    // Refresh list of queues from the selected region.
    $selectedQueue = $config->get('aws_queue_name') ?? $queue->get('aws_queue_name');
    if ($form_state->getValue('region')) {
      $region = $form_state->getValue('region');
    }
    else {
      $region = $config->get('region') ?? $queue->get('region');
    }
    $queuesAvailable = $this->awsInformation->listQueues($region);

    $defaultValue = (in_array($selectedQueue, $queuesAvailable)) ? $selectedQueue : NULL;
    $form['queue_name'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Queue'),
    ];

    $nameIsRequired = $this->config(GlobalDefaultsConfigurationForm::CONFIG_KEY)->get('naming') !== Configuration::NAME_OPTIONAL;
    $form['queue_name']['aws_queue_name'] = [
      '#type' => 'select',
      '#title' => $this->t('SQS queue name'),
      '#options' => array_combine($queuesAvailable, $queuesAvailable),
      '#default_value' => $defaultValue,

      // The global configuration may allow the SQS queue name to be
      // automatically defined using the Drupal queue worker plugin ID.
      '#empty_value' => '',
      '#required' => $nameIsRequired,

      // Populate a prefix and suffix to interact with the region ajax model.
      '#prefix' => '<div id="select-queue">',
      '#suffix' => '</div>',
    ];
    if (!$nameIsRequired) {
      $form['queue_name']['aws_queue_name']['#empty_option'] = $this->t('- Use Drupal queue name -');
    }

    $form['queue_processing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Queue processing'),
    ];
    $form['queue_processing']['lease_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Lease time'),
      '#description' => $this->t("Lease time defines how long a queue item can be reserved for processing before it's released for another consumer to attempt."),
      '#options' => $this->leaseTimeOptions(),
      '#default_value' => $config->get('lease_time') ?? $queue->get('lease_time'),
    ];

    $form['queue_processing']['wait_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Wait time'),
      '#description' => $this->t("Wait time defines how long a process can wait for a message to arrive during polling. Maximum 30 seconds."),
      '#options' => $this->waitTimeOptions(),
      '#default_value' => $config->get('wait_time') ?? $queue->get('wait_time'),
    ];

    $form = parent::buildForm($form, $form_state);

    $form['actions']['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => new Url('aws_sqs_api.admin_overview'),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Compare saved config with the annotation, and ignore matches.
    $config = $this->config(self::CONFIG_KEY_PREFIX . '.' . $this->queue->id());
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      if ($key === 'config_key') {
        continue;
      }
      $config->set($key, $value);
    }
    $config->save();

    // Return to the overview form.
    $form_state->setRedirect('aws_sqs_api.admin_overview');

    return parent::submitForm($form, $form_state);
  }

  /**
   * Ajax callback to refresh the queue list for a given region.
   */
  public function ajaxRefreshQueues(array &$form, FormStateInterface $form_state) {
    // Simply return the form element with the queues.
    // The form behaviour will already refresh the list.
    return $form['queue_name']['aws_queue_name'];
  }

}
