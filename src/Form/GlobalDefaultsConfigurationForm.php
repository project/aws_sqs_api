<?php

namespace Drupal\aws_sqs_api\Form;

use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\aws_sqs_api\Queue\Configuration;
use Drupal\aws_sqs_api\Service\AwsInformation;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide configuration for global behaviour and defaults.
 */
class GlobalDefaultsConfigurationForm extends ConfigFormBase {

  use FormOptionsTrait;

  /**
   * Config key which will store the configuration.
   *
   * @var string
   */
  const CONFIG_KEY = 'aws_sqs_api.default_settings';

  /**
   * AWS information service.
   *
   * This is used to query a list of regions.
   *
   * @var \Drupal\aws_sqs_api\Service\AwsInformation
   */
  protected $awsInformation;

  /**
   * Construct a configuration form for SQS defaults.
   *
   * @param \Drupal\aws_sqs_api\Service\AwsInformation $aws_information
   *   AWS information service.
   */
  public function __construct(AwsInformation $aws_information) {
    $this->awsInformation = $aws_information;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('aws_sqs_api.aws_information')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_sqs_api.global_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      self::CONFIG_KEY,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);

    $form['region'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Region'),
    ];
    $regions = $this->awsInformation->listRegions();
    $form['region']['region'] = [
      '#type' => 'select',
      '#title' => $this->t('Default AWS region'),
      '#options' => array_combine($regions, $regions),
      '#default_value' => $config->get('region') ?? AwsSqsQueue::DEFAULT_REGION,
    ];

    $form['queue_processing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Queue processing'),
    ];
    $form['queue_processing']['lease_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Lease time'),
      '#description' => $this->t("Lease time defines how long a queue item can be reserved for processing before it's released for another consumer to attempt."),
      '#options' => $this->leaseTimeOptions(),
      '#default_value' => $config->get('lease_time') ?? AwsSqsQueue::DEFAULT_LEASE_TIME,
    ];
    $form['queue_processing']['wait_time'] = [
      '#type' => 'select',
      '#title' => $this->t('Wait time'),
      '#description' => $this->t("Wait time defines how long a process can wait for a message to arrive during polling. Maximum 30 seconds."),
      '#options' => $this->waitTimeOptions(),
      '#default_value' => $config->get('wait_time') ?? AwsSqsQueue::DEFAULT_WAIT_TIME,
    ];

    $form['queue_management'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('SQS queue management'),
    ];
    $form['queue_management']['naming'] = [
      '#type' => 'radios',
      '#title' => $this->t('Queue naming'),
      '#options' => [
        Configuration::NAME_REQUIRED => $this->t('Required: an existing SQS queue must be selected.'),
        Configuration::NAME_OPTIONAL => $this->t('Optional: Use the ID of the Drupal queue-worker plugin as the name of the SQS queue.'),
      ],
      '#default_value' => $config->get('naming') ?? Configuration::NAME_REQUIRED,
    ];
    $form['queue_management']['autocreate'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Create SQS queue if it doesn't exist."),
      '#description' => $this->t('Only available when queue-naming is optional.')
      . '<br />'
      . $this->t('This feature may require additional AWS permissions.'),
      '#default_value' => $config->get('autocreate') ?? FALSE,
      // Enable auto-create only when SQS queue-naming is optional.
      '#states' => [
        'enabled' => [
          ':input[name="naming"]' => ['value' => Configuration::NAME_OPTIONAL],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_KEY);
    $form_state->cleanValues();
    foreach ($form_state->getValues() as $key => $value) {
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
