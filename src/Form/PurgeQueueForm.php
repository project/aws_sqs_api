<?php

namespace Drupal\aws_sqs_api\Form;

use Drupal\aws_sqs_api\Queue\AwsSqsQueue;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Confirmation form displayed when purging an SQS queue.
 */
class PurgeQueueForm extends ConfirmFormBase {

  /**
   * AWS SQS queue implementation.
   *
   * @var \Drupal\aws_sqs_api\Queue\AwsSqsQueue
   */
  protected $queue;

  /**
   * Machine name of the Drupal queue. Inferred from the queue config.
   *
   * @var string
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, AwsSqsQueue $queue = NULL) {
    $this->queue = $queue;
    $this->id = $queue->label();
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Purge the queue and provide user-feedback.
    $result = $this->queue->purge();
    if ($result === TRUE) {
      $this->messenger()->addStatus($this->t('The %id queue has been purged.', ['%id' => $this->id]));
    }
    else {
      $this->messenger()->addWarning($this->t('Failed to purge the %id queue: %error', [
        '%id' => $this->id,
        '%error' => $result,
      ]));
    }
    $form_state->setRedirect('aws_sqs_api.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'aws_sqs_api.purge_queue_confirmation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('aws_sqs_api.admin_overview');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to purge queue %id?', ['%id' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('All entries in queue %id will be permanently removed.', ['%id' => $this->id]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Purge queue');
  }

}
