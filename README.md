# Quickstart

1. Identify the queues which should be backed by SQS.
   Record the system ID of each queue worker plugin.

2. Configure the queue to use the SQS API in `settings.php`.

   ```php
   // Configure queue worker "my_event_bus" to use SQS.
   $settings['queue_service_my_event_bus'] = 'queue.aws_sqs';
   ```

3. Provide AWS Access Keys which have permissions to use the queue.
   See the [AWS Developer Guide](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html).
